# -*- coding: cp1250 -*-
"""
Representation of a collection of SW equations, with constructor methods to
import SW equations by:
 - copying from the equation manager window in SolidWorks.
 - parsing the extracted raw data collected by the SW wrapper.
"""
import attr
from typing import List, Tuple

from sw_eq_parser.sw_equation import SWEquation, SWVariable, VarOptValue


@attr.s(auto_attribs=True)
class SWEquationCollection:
    """Representation of a group of SW equations."""

    equations: List[SWEquation] = attr.ib(factory=list)

    @classmethod
    def parse_from_copied_rows(cls, raw: str):
        """
        Parse a group of equations in SW syntax.

        Ready to parse lines from a manual copy + paste operation from the
        SW equation manager window, after selection of the desired rows.

        Format looks like:
        '''
        "fan_L2"	= 465	465	1
        "D2@Z�kladn� plech1"	= "fan_L2"	465mm	0
        "D3@LinPole1"	= "D1@Skica11"	0mm	0
        "D3@Skica11"	= "D5@DIME@N015U-01-00&DV&DV 70<1>.Assembly"		0
        Tlou��ka	 = 2	2mm	0
        '''
        """
        valid_eqs = filter(lambda x: len(x.strip()) > 0, raw.splitlines())
        return cls(
            equations=list(map(SWEquation.parse_from_copied_line, valid_eqs))
        )

    @classmethod
    def parse_from_collected_data(
        cls, extracted: List[Tuple[str, VarOptValue]]
    ):
        """
        Parse a group of equations in SW syntax.

        Ready to parse equations from the extracted tuples obtained in the data
        collection with the SW wrapper.

        Format looks like:
        '''
        [
            ['"D2@Z�kladn� plech1"= "VYSKA_VNE" - 2 * 30', 1260.0],
            ['"SUP_KLP"=269', 269.0],
            ['"ETA_KLP"=281', 281.0],
            ['"EHA_KLP"=265', 265.0],
            ['"ODA_KLP"=300', 300.0],
            ['"VYSKA_VNE"=1320.0', 1320.0]
        ]
        '''
        """
        return cls(
            equations=list(map(SWEquation.parse_from_collected, extracted))
        )

    def solve_equations(
        self, known_vars: dict, update_eval: bool = True, verbose: bool = False
    ) -> List[SWEquation]:
        """
        Try to solve the maximum number of equations contained in the system.

        Uses a passed dict with already known variable - value pairs and
        updates it with solved variables in system.

        At the end, return subset of unsolved equations.

        Optionally (but the default behaviour), it updates the `eval_value`
        field with the last solved value.
        """
        unsolved_eqs = [eq for eq in self.equations]
        stop = False
        while len(unsolved_eqs) > 0 and not stop:
            stop = True
            for i, eq in enumerate(unsolved_eqs):
                try:
                    result = eq.solve(
                        known_vars, strict=True, update_eval=update_eval
                    )
                    known_vars[eq.variable.expr_name] = result
                    if verbose:  # pragma: no cover
                        print(
                            f"Result #{len(known_vars)} / "
                            f"left: {len(unsolved_eqs) - 1} "
                            f"for {eq.variable.original_name} "
                            f"-> {result} [{type(result).__name__}]; "
                            f" == {eq.old_value} ?"
                        )
                    res_check = SWVariable.parse(str(eq.eval_value))
                    assert res_check.has_value
                    if res_check.value != result:  # pragma: no cover
                        # print(eq.eval_value, eq.raw_value)
                        # eq.render()
                        raise ValueError(
                            f"Different result: {res_check.original_name} "
                            f"= {eq.eval_value} != {eq.raw_value}"
                        )

                    unsolved_eqs.pop(i)
                    stop = False
                    break
                except ValueError:
                    pass

        return unsolved_eqs

    def show_results(self):
        """Pretty printing of equation values."""
        unsolved = len(
            list(filter(lambda x: x.eval_value is None, self.equations))
        )
        print(f"* SYSTEM WITH {len(self.equations)} EQS. {unsolved} unsolved!")
        print("=" * 100)
        for eq in self.equations:
            value = "???" if eq.eval_value is None else eq.eval_value
            print(
                f"\t{eq.variable.original_name:55s}={value} == {eq.old_value}"
            )
        print("-" * 100)

    def __len__(self):
        """Measure number of equations in collection."""
        return len(self.equations)

    def __iter__(self):
        """Iterate over equations in collection."""
        for eq in self.equations:
            yield eq

    def __getitem__(self, key):
        """
        Multipurpose access to equations in collection
        by variable name (case insensitive, '"' insensitive) and by index.
        """
        if isinstance(key, int):
            return self.equations[key]
        for eq in self:
            if key.lower().strip('"') == eq.variable.original_name.lower():
                return eq
        raise KeyError(
            f"Equation for '{key}' not found in system ({len(self)})"
        )

    def __contains__(self, other) -> bool:
        """
        Test presence in equation collection for:
        - Single equations (SWEquation)
        - Other eq. collections, test if subset (SWEquationCollection)
        - Variable definition (SWVariable)
        - Variable definition by name (str)
        """
        if isinstance(other, SWEquation):
            return other in self.equations
        elif isinstance(other, SWEquationCollection):
            return all(eq_other in self for eq_other in other.equations)
        elif isinstance(other, str):
            try:
                _ = self[other]
                return True
            except KeyError:
                return False
        elif isinstance(other, SWVariable):
            return any(other == eq.variable for eq in self.equations)
        return False

"""Package installation for SolidWorks Equation Parser."""
from pathlib import Path
import re

from pkg_resources import parse_requirements
from setuptools import setup

PACKAGE = "sw_eq_parser"

ROOT_DIR = Path(__file__).parent
REQUIREMENTS_DIR = ROOT_DIR / "requirements"
VERSION_RE = re.compile(r'__version__ = "([^"]+)"')


def _read_version(path):
    found = VERSION_RE.findall(path.read_text())
    return found[0] if found else "0.0.0a0"


def _read_pip_file(path: Path):
    with open(str(path), "r") as f:
        return list(map(str, parse_requirements(f.read())))


long_description = (ROOT_DIR / "README.md").read_text()

setup(
    name=PACKAGE,
    version=_read_version(ROOT_DIR / PACKAGE / "__init__.py"),
    packages=[PACKAGE],
    package_dir={PACKAGE: PACKAGE},
    url="https://gitlab.com/remak-backend/proposals/sw-eq-parser",
    license="MIT",
    author="TBE Team",
    author_email="contact-tbe-team@googlegroups.com",
    maintainer="Eugenio Panadero",
    maintainer_email="eugenio.panadero@gmail.com",
    description="SolidWorks Equation parser",
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=_read_pip_file(REQUIREMENTS_DIR / "main.pip"),
    extras_require={"dev": _read_pip_file(REQUIREMENTS_DIR / "dev.pip")},
)

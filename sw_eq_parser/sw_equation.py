# -*- coding: cp1250 -*-
"""Simple equation parser to analyze SW parametric variables."""
from copy import deepcopy
from enum import Enum
from functools import reduce
from math import copysign, exp, log, sqrt, pi
import logging
import re
from typing import Dict, List, Optional, Tuple, Union

from anytree import AnyNode, RenderTree, AsciiStyle
import attr

# TODO Link with variable_importer script

# Master flags
USE_NULL_VALUE_FOR_UNASSIGNED_VARS = True
NULL_VARIABLE_NAME = "NULL"


# Regular expressions to parse SW syntax
# fmt: off
RG_CLEAN_PARTIAL_EQ = re.compile(r"^((\s*)?=(\s*)?)")
RG_IF_COND = re.compile(
    r"\s*?iif\s*?\((?P<cond>[^,()]*)\s*?,"
    r"\s*?(?P<v_true>[^,()]*)\s*?,\s*?(?P<v_false>[^,()]*)\s*?\)"
)
RG_SUBEXPR = re.compile(r"(?P<func>\w+)?(\s*)?\((\s*)?"
                        r"(?P<subexpr>[^,()]*)(\s*)?\)")
RG_NUMBER_WITH_UNIT = re.compile(r"^(?P<value>-?[0-9.]+)(?P<unit>\w{1,2})?$")
RG_VARIABLE = re.compile(r'("([^"@=,/+]*)@?([^"=,/+]*)?")')
RG_COMPARATORS = re.compile(
    r"[^a-zA-Z_<>=!.](?P<comp>[<>=!&]+)[^a-zA-Z_<>=!.]"
)
RG_OPERATORS = re.compile(
    r"(?:^|\d|(?:[^\w_<>]\s?))(?P<op>[/+\-*\^])\s?(?:[^\w_<>=!.])?"
)
RG_MUL_OPERS = re.compile(r"([/*\^]|&&)")
RG_ADD_OPERS = re.compile(r"([+\-])")
RG_VARIABLE_SLUG = re.compile(r'("(?:[^"@=/]*)@?(?:[^"=/]*)?")')
RG_SUBSTITUTE_AND_OPERATOR = re.compile(r'[^"\w](AND)[^"\w]')
RG_SUBSTITUTE_GTE_OPERATOR = re.compile(r'[^"\w](=\s+?>)[^"\w]')
RG_SUBSTITUTE_NOT_EQUAL_OPERATOR = re.compile(r'[^"\w](<\s+?>)[^"\w]')
INTERNAL_OP_AND = "&&"
INTERNAL_OP_GTE = ">="
INTERNAL_OP_NOT_EQUAL = "!="

PREC_FLOAT_DECS_FINAL = 3

# TODO add trigonometric funcs (define base first: degrees vs radians)
# Function
# sin (a)  sine  a is the angle; returns the sine ratio
# cos (a)  cosine  a is the angle; returns the cosine ratio
# tan (a)  tangent  a is the angle; returns the tangent ratio
# sec (a)  secant  a is the angle; returns the secant ratio
# cosec (a)  cosecant  a is the angle; returns the cosecant ratio
# cotan (a)  cotangent  a is the angle; returns the cotangent ratio
# arcsin (a)  inverse sine  a is the sine ratio; returns the angle
# arccos (a)  inverse cosine  a is the cosine ratio; returns the angle
# atn (a)  inverse tangent  a is the tangent ratio; returns the angle
# arcsec (a)  inverse secant  a is the secant ratio; returns the angle
# arccosec (a)  inverse cosecant  a is the cosecant ratio; returns the angle
# arccotan (a)  inverse cotangent  a is the cotangent ratio; returns the angle

KNOWN_FUNCS = {
    "int": int,
    "abs": abs,
    "exp": exp,
    "log": log,
    "sqr": sqrt,
    "sgn": lambda x: copysign(1, x),
    # "custom": _python_method,
}
KNOWN_CONSTANTS = {
    "pi": pi,
    # "CUSTOM_CONSTANT": 1.234,
}
UNIT_CONVERSIONS = {
    "mm": 1,
}
SPECIAL_VALUES = ["suppressed", "unsuppressed"]
if USE_NULL_VALUE_FOR_UNASSIGNED_VARS:
    SPECIAL_VALUES.append(NULL_VARIABLE_NAME)
# fmt: on

# Typing used
VarOptValue = Optional[Union[int, float, str, bool]]
VarValue = Union[int, float, str, bool]


# Helper methods to rename variables for easier internal usage
def to_slug(raw_name: str) -> str:
    """
    Simplified internal repr for var names, to not mess with the parser.

    Reason to be of this _slugifying_  is because variable names as
    "D5@DIME@N015U-01-00&DV&DV 70<1>.Assembly" are valid ones.
    """
    return (
        raw_name.replace("<", "[")
        .replace(">", "]")
        .replace(" ", ";")
        .replace("-", "__")
        .replace(",", ";;")
    )


def _variable_name_cleaning(raw_text: str) -> str:
    return RG_VARIABLE_SLUG.sub(lambda m: to_slug(m.group()), raw_text)


def to_original(slug_name: str) -> str:
    """Reconstruction of original name from slugified one."""
    return (
        slug_name.replace("[", "<")
        .replace("]", ">")
        .replace(";;", ",")
        .replace(";", " ")
        .replace("__", "-")
    )


class SWExprType(str, Enum):
    """Different kinds of implemented expressions."""

    VALUE = "X"
    SPECIAL_VALUE = "S"
    VARIABLE = "V"

    CONDITIONAL = "IF"

    ADDITION = "+"
    SUBTRACTION = "-"
    MULTIPLICATION = "*"
    DIVISION = "/"
    POWER = "^"

    EQUAL = "="
    NOT_EQUAL = INTERNAL_OP_NOT_EQUAL
    GREATER = ">"
    EQUAL_OR_GREATER = INTERNAL_OP_GTE
    LOWER = "<"

    AND = INTERNAL_OP_AND

    @property
    def is_sum(self):
        return self.value in [SWExprType.ADDITION, SWExprType.SUBTRACTION]

    @property
    def is_operation(self):
        return self.value in [
            SWExprType.ADDITION,
            SWExprType.SUBTRACTION,
            SWExprType.MULTIPLICATION,
            SWExprType.DIVISION,
            SWExprType.POWER,
        ]

    @property
    def is_comparison(self):
        return self.value in [
            SWExprType.EQUAL,
            SWExprType.NOT_EQUAL,
            SWExprType.EQUAL_OR_GREATER,
            SWExprType.GREATER,
            SWExprType.LOWER,
            SWExprType.AND,
        ]


# TODO Implement parsing of assembly syntax (from SW API help):
#  The equation syntax for references between assembly components loads
#  automatically when you select dimensions, features, and global variables
#  in the FeatureManager design tree, the graphics area, File Properties,
#  and the Equations dialog box.
#  Existing equations that do not follow this syntax are marked as errors
#  because they create inconsistent results. You need to edit these equations
#  to correct the syntax.
#
# In the following example, A1 is a subassembly of A2 containing
# the parts P1 and P2:
#
# GV1 and GV2 are global variables in the documents. Use the following syntax:
#
# Edit Target Reference to Syntax
# A2 A2 �GV1� = �GV2�
# A2 A1 �GV1� = �GV2@A1<1>.Assembly
# A2 P1 �GV1� = �GV2@P1<1>.Part@A1<1>.Assembly
# A1 A2 �GV1� = �GV2@A2.Assembly�
# A1 A1 �GV1� = �GV2�
# A1 P1 �GV1� = �GV2@P1<1>.Part�
# P1 A2 �GV1� = �GV2@A2.Assembly�
# P1 A1 �GV1� = �GV2@A1<1>.Assembly�
# P1 P1 �GV1� = �GV2�
# P1 P2 �GV1� = �GV2@P2<1>.Part@A1<1>.Assembly�


@attr.s(auto_attribs=True, repr=False)
class SWVariable:
    """Representation of variables in SolidWorks syntax."""

    raw_name: str = attr.ib(factory=str)
    name: str = attr.ib(factory=str)
    location: str = attr.ib(factory=str)
    value: VarOptValue = attr.ib(default=None)

    def __repr__(self):  # pragma: no cover
        # return self.raw_name
        return self.original_name

    def __attrs_post_init__(self):
        if self.name in SPECIAL_VALUES:
            self.value = f'"{self.name}"'

    @property
    def has_value(self) -> bool:
        """Check if variable is a constant value."""
        return self.value is not None

    @property
    def variable_type(self) -> SWExprType:
        """
        Return expression type for variable.
        (can be value, special value, or symbolic variable)
        """
        if self.value is None:
            return SWExprType.VARIABLE
        elif self.name in SPECIAL_VALUES:
            return SWExprType.SPECIAL_VALUE
        return SWExprType.VALUE

    def eval_value(self, known_expr: dict, strict: bool = False) -> VarValue:
        """
        Get variable value by accessing the constant value or looking into
        a collection of known variable values.
        """
        if self.value is not None:
            return self.value
        elif to_original(self.expr_name) in known_expr:
            value = known_expr[to_original(self.expr_name)]
            return value
        elif not strict:
            logging.warning(
                f"Unknown variable: {self.expr_name}. "
                f"Not in {list(known_expr.keys())}. Using 0.0 for that."
            )
            return 0.0
        raise ValueError(f"Can't eval unknown variable {self.expr_name}")

    @property
    def expr_name(self):
        """Return variable name in SW syntax without the '"' to be composed."""
        if self.location:
            return f"{self.name}@{self.location}"
        return self.name

    @property
    def original_name(self):
        """Return variable name in SW syntax without the '"' to be composed."""
        return to_original(self.expr_name)

    @classmethod
    def parse(cls, raw: str):
        """Parse raw variable definition in SW syntax."""
        raw = _variable_name_cleaning(raw)
        found = RG_VARIABLE.findall(raw)
        if found:
            return cls(*found[0])

        raw_s = raw.strip()
        if not raw_s:
            msg = f"Empty variable encountered: '{raw}'"
            # logging.warning(msg)
            if USE_NULL_VALUE_FOR_UNASSIGNED_VARS:
                return cls(
                    raw_name=NULL_VARIABLE_NAME,
                    name=NULL_VARIABLE_NAME.strip('"'),
                )
            raise ValueError(msg)  # pragma: no cover
        elif raw_s in SPECIAL_VALUES:
            return cls(raw_name=raw_s, name=raw_s)
        elif raw_s in KNOWN_CONSTANTS:
            return cls(
                raw_name=raw_s, name=raw_s, value=KNOWN_CONSTANTS[raw_s]
            )

        search_num = RG_NUMBER_WITH_UNIT.search(raw_s)
        if search_num is None:
            raise ValueError(f"BAD number + unit parsing in '{raw}'")
        found_n = search_num.groupdict()
        raw_value = found_n["value"]
        conv_factor = 1
        if found_n["unit"] is not None and found_n["unit"] in UNIT_CONVERSIONS:
            conv_factor = UNIT_CONVERSIONS[found_n["unit"]]
        try:
            value = int(raw_value) * conv_factor
            return cls(raw, name=f"i{value:.0f}", location="", value=value)
        except ValueError:
            try:
                fval = float(raw_value) * conv_factor
                return cls(raw, name=f"f{fval:.0f}", location="", value=fval)
            except ValueError as exc:
                raise ValueError(f"BAD variable parsing: {raw} -> {exc}")


class SWExpressionTree(AnyNode):
    """Recursive representation of an equation in SolidWorks syntax.

    - Each node represents an operation, or a symbolic variable, or some value.
    - The node can be negated (`x * (-1)`)
    - The node can have a known function applied (`int(x)`)
    - When the argument order (:= children nodes order) in the parent
    expression is needed (:= A / B != B / A), it is present in `.arg_order`
    - Finally, each node stores its last evaluated value when `.solve()` is
    called with variable values.
    """

    raw_text: str = ""
    ex_type: SWExprType = SWExprType.VARIABLE
    variable: Optional[SWVariable] = None
    negate: bool = False
    func_apply: Optional[List[str]] = None
    arg_order: int = 0
    last_eval_value: Optional[VarValue] = None

    def __repr__(self):  # pragma: no cover
        extra = "(-)" if self.negate else ""
        if self._has_func_applied():
            f_apply = "_".join(self.func_apply)
            type_repr = f"{extra}{f_apply}({self.ex_type.name})"
        else:
            type_repr = f"{extra}{self.ex_type.name}"
        str_repr = f"SWExpr[{type_repr}] -> '{self.raw_text}'"
        if self._is_simple_expr():
            str_repr += f"; V={self.variable}"
            if self.ex_type == SWExprType.VALUE:
                str_repr += f", type: {type(self.variable.value).__name__}"
        str_repr += ")"
        if self.last_eval_value is not None:
            str_repr += f" ==> {self.last_eval_value:g}"

        return str_repr

    @staticmethod
    def _sort_subexprs(items):
        return list(sorted(items, key=lambda x: x.arg_order))

    def _children_args(self) -> List["SWExpressionTree"]:
        return list(self._sort_subexprs(self.children))

    def __eq__(self, other: "SWExpressionTree"):
        if (
            self.raw_text != other.raw_text
            or self.variable != other.variable
            or self.ex_type != other.ex_type
            or self.negate != other.negate
            or self.func_apply != other.func_apply
            or self.arg_order != other.arg_order
            # self.last_eval_value != other.last_eval_value
        ):
            return False
        for ch, other_ch in zip(self._children_args(), other._children_args()):
            if ch != other_ch:  # pragma: no cover
                return False
        return True

    def render(self):  # pragma: no cover
        """Pretty representation of the assembly tree."""
        kwargs = dict(style=AsciiStyle(), childiter=self._sort_subexprs)
        print(RenderTree(self, **kwargs))

    @classmethod
    def _rec_parse(
        cls,
        eq: str,
        known: Dict[str, "SWExpressionTree"],
        is_comp: bool = False,
        parent: Optional["SWExpressionTree"] = None,
    ):
        """Recursive constructor of equation terms."""
        # Initial cleaning of sub-expression
        eq = eq.strip()

        if eq in known:
            known_subtree = deepcopy(known[eq])
            known_subtree.parent = parent
            return known_subtree

        # Look for subexpressions or function calls
        search_sub_expr = RG_SUBEXPR.search(eq)
        if search_sub_expr is not None:
            found = search_sub_expr.groupdict()
            child_node = cls._rec_parse(found["subexpr"], known)
            if found["func"]:
                if child_node.func_apply is None:
                    child_node.func_apply = []
                child_node.func_apply.append(found["func"].strip().lower())
            substitute = f'"{child_node.expr_name}"'
            known[substitute] = child_node
            return cls._rec_parse(
                RG_SUBEXPR.sub(substitute, eq, count=1), known, parent=parent
            )
        elif "iif" in eq:  # SWExprType.CONDITIONAL found
            search_cond_data = RG_IF_COND.search(eq)
            if search_cond_data is None:  # pragma: no cover
                raise ValueError(f"Can't parse conditional value in {eq}")
            data = search_cond_data.groupdict()
            cond_n = cls(raw_text=eq, ex_type=SWExprType.CONDITIONAL)
            cls._rec_parse(data["cond"], known, True, parent=cond_n)
            cls._rec_parse(data["v_true"], known, parent=cond_n).arg_order = 1
            cls._rec_parse(data["v_false"], known, parent=cond_n).arg_order = 2
            substitute = f'"{cond_n.expr_name}"'
            known[substitute] = cond_n
            return cls._rec_parse(
                RG_IF_COND.sub(substitute, eq, count=1), known, parent=parent
            )

        # Search for comparators '>', '<', '='
        search_comp_data = RG_COMPARATORS.search(eq)
        if is_comp or search_comp_data is not None:
            # if search_comp_data is None:  # pragma: no cover
            #     raise ValueError(f"Can't parse comparator expression: {eq}")
            comp = search_comp_data.groupdict()["comp"]
            first, second = eq.split(comp, maxsplit=1)
            pnode = cls(raw_text=eq, ex_type=SWExprType(comp), parent=parent)
            cls._rec_parse(first, known, parent=pnode)
            cls._rec_parse(second, known, parent=pnode).arg_order = 1
            return pnode

        # Search for Arithmetic operators (+, -, *, /)
        found_ops = RG_OPERATORS.findall(eq)
        if not found_ops:
            # Found simple expresion
            var = SWVariable.parse(eq)
            return cls(
                raw_text=eq,
                ex_type=var.variable_type,
                variable=var,
                parent=parent,
            )

        elif len(found_ops) > 1 and len(RG_ADD_OPERS.findall(eq)) > 1:
            # Multiple factor addition/subtraction
            raw_split = RG_ADD_OPERS.split(eq)
            if not raw_split[0]:
                raw_split.pop(0)
            if len(raw_split) % 2 == 0:
                raw_signs = raw_split[::2]
                raw_factors = raw_split[1::2]
            else:
                raw_factors = raw_split[::2]
                raw_signs = ["+"] + raw_split[1::2]

            pnode = cls(
                raw_text=eq, ex_type=SWExprType.ADDITION, parent=parent
            )
            for factor, raw_sign in zip(raw_factors, raw_signs):
                ch = cls._rec_parse(factor, known, parent=pnode)
                ch.negate = SWExprType(raw_sign) == SWExprType.SUBTRACTION
            return pnode

        # Left / right normal operation
        if len(found_ops) == 1:
            op = found_ops[0]
            raw_left, raw_right = eq.split(op)
        elif RG_MUL_OPERS.findall(eq) and RG_ADD_OPERS.findall(eq):
            # parse addition factors in pairs
            raw_left, op, raw_right = RG_ADD_OPERS.split(eq, maxsplit=1)
        else:
            op = found_ops[0]
            args = eq.split(op)
            raw_left = op.join(args[:-1])
            raw_right = args[-1]

        pnode = cls(raw_text=eq, ex_type=SWExprType(op), parent=parent)
        cls._rec_parse(raw_left, known, parent=pnode)
        cls._rec_parse(raw_right, known, parent=pnode)
        return pnode

    @classmethod
    def parse(
        cls,
        eq: str,
        known_subs: Dict[str, "SWExpressionTree"] = None,
        parent: Optional["SWExpressionTree"] = None,
    ):
        """Create SWExpression from raw equation value."""
        if known_subs is None:
            known_subs = {}

        # Apply operator substitutions
        clean_eq = reduce(
            lambda x, y: y[0].sub(y[1], x),
            [
                _variable_name_cleaning(eq),
                (RG_SUBSTITUTE_AND_OPERATOR, INTERNAL_OP_AND),
                (RG_SUBSTITUTE_NOT_EQUAL_OPERATOR, INTERNAL_OP_NOT_EQUAL),
                (RG_SUBSTITUTE_GTE_OPERATOR, INTERNAL_OP_GTE),
            ],
        )

        # Run recursive parsing
        return cls._rec_parse(clean_eq, known_subs, parent=parent)

    @property
    def expr_name(self):
        """Use to generate a new variable name for complex expressions."""
        f_prefix = ""
        if self._has_func_applied():
            f_prefix = "_".join(self.func_apply) + "_"

        if self.ex_type == SWExprType.CONDITIONAL:
            cond, v_true, v_false = self._children_args()
            return (
                f"{f_prefix}cond_{cond.expr_name}"
                f"__t_{v_true.expr_name}__f_{v_false.expr_name}"
            )

        elif self.ex_type.is_sum:

            def _f_mark(x):
                return "p" if x == SWExprType.ADDITION else "m"

            if len(self.children) > 2:
                factors = map(
                    lambda x: f"{_f_mark(self.ex_type)}_{x.expr_name}",
                    self._children_args(),
                )
                return f_prefix + f"__s__".join(factors)
            else:
                left_var, right_var = self._children_args()
                factors = [left_var.expr_name, right_var.expr_name]
                return f_prefix + f"__{_f_mark(self.ex_type)}__".join(factors)

        elif self.ex_type.is_operation:
            left_var, right_var = self._children_args()
            if self.ex_type == SWExprType.MULTIPLICATION:
                tag = "mul"
            elif self.ex_type == SWExprType.POWER:
                tag = "pow"
            else:  # if self.ex_type == SWExprType.DIVISION:
                tag = "div"
            return (
                f"{f_prefix}{tag}_{left_var.expr_name}__{right_var.expr_name}"
            )

        elif self.ex_type.is_comparison:
            left_var, right_var = self._children_args()
            if self.ex_type == SWExprType.EQUAL:
                tag = "eq"
            elif self.ex_type == SWExprType.NOT_EQUAL:
                tag = "neq"
            elif self.ex_type == SWExprType.GREATER:
                tag = "gt"
            elif self.ex_type == SWExprType.EQUAL_OR_GREATER:
                tag = "gte"
            elif self.ex_type == SWExprType.AND:
                tag = "and"
            else:  # if self.ex_type == SWExprType.LOWER:
                tag = "lt"
            return (
                f"{f_prefix}{left_var.expr_name}__{tag}__{right_var.expr_name}"
            )

        return f"{f_prefix}{self.variable.expr_name}"

    def _is_simple_expr(self) -> bool:
        return self.ex_type in [
            SWExprType.VARIABLE,
            SWExprType.VALUE,
            SWExprType.SPECIAL_VALUE,
        ]

    def _has_func_applied(self) -> bool:
        return self.func_apply is not None and len(self.func_apply) > 0

    def _apply_func(self, value):
        if self._has_func_applied():
            try:
                value = reduce(
                    lambda x, y: KNOWN_FUNCS[y](x), [value, *self.func_apply]
                )
            except KeyError:  # pragma: no cover
                logging.critical(
                    f"ERROR: Unknown function to apply: "
                    f"'{'+'.join(self.func_apply)}'. "
                    f"Known funcs: {KNOWN_FUNCS.keys()}"
                )
        if self.negate:
            value *= -1
        self.last_eval_value = value
        return value

    def get_variables(self) -> List[SWVariable]:
        """Collect variable objects used inside the expression."""
        if self.variable is not None and self._is_simple_expr():
            return [self.variable]
        else:
            return [
                v for ch in self._children_args() for v in ch.get_variables()
            ]

    @property
    def involved_variables_names(self) -> List[str]:
        """Collect names of variables used inside the expression."""
        return list(
            sorted(
                set(
                    v.original_name
                    for v in self.get_variables()
                    if not v.has_value
                ),
                key=lambda x: x.lower(),
            )
        )

    def _solve_children(
        self, expected_args: int, values: dict, strict: bool = False
    ) -> List[VarValue]:
        args = list(
            map(
                lambda x: x.solve(values, strict=strict), self._children_args()
            )
        )
        assert len(args) == expected_args
        return args

    def solve(self, variable_values: dict, strict: bool = False) -> VarValue:
        """Solve expression using an external dict of values for variables."""
        if self.variable is not None and self._is_simple_expr():
            return self._apply_func(
                self.variable.eval_value(variable_values, strict=strict)
            )

        elif self.ex_type == SWExprType.CONDITIONAL:
            condition, true_value, false_value = self._solve_children(
                expected_args=3, values=variable_values, strict=strict
            )
            if condition:
                return self._apply_func(true_value)
            return self._apply_func(false_value)

        elif len(self.children) > 2:
            assert self.ex_type == SWExprType.ADDITION
            total_value = 0
            for factor in self._children_args():
                f_value = factor.solve(variable_values, strict=strict)
                assert isinstance(f_value, float) or isinstance(f_value, int)
                total_value += f_value

            return self._apply_func(total_value)

        left_value, right_value = self._solve_children(
            expected_args=2, values=variable_values, strict=strict
        )

        if self.ex_type.is_operation:
            assert not isinstance(left_value, str)
            assert not isinstance(right_value, str)
            if self.ex_type == SWExprType.ADDITION:
                return self._apply_func(left_value + right_value)
            elif self.ex_type == SWExprType.SUBTRACTION:
                return self._apply_func(left_value - right_value)
            elif self.ex_type == SWExprType.MULTIPLICATION:
                return self._apply_func(left_value * right_value)
            elif self.ex_type == SWExprType.POWER:
                return self._apply_func(pow(left_value, right_value))
            elif self.ex_type == SWExprType.DIVISION:
                try:
                    return self._apply_func(left_value / right_value)
                except ZeroDivisionError:  # pragma: no cover
                    logging.critical(
                        f"ERROR: ZeroDivisionError evaluating {self.raw_text}:"
                        f" denominator is ZERO!. Using 1."
                    )
                    return self._apply_func(left_value)

        else:
            assert self.ex_type.is_comparison
            if self.ex_type == SWExprType.EQUAL:
                return left_value == right_value
            elif self.ex_type == SWExprType.NOT_EQUAL:
                return left_value != right_value
            elif self.ex_type == SWExprType.GREATER:
                return left_value > right_value
            elif self.ex_type == SWExprType.EQUAL_OR_GREATER:
                return left_value >= right_value
            elif self.ex_type == SWExprType.AND:
                return self._apply_func(left_value and right_value)
            else:  # elif self.ex_type == SWExprType.LOWER:
                return left_value < right_value


@attr.s(auto_attribs=True)
class SWEquation:
    """Representation of equations in the form `SWVariable = SWExpression`."""

    variable: SWVariable = attr.ib(factory=SWVariable)
    raw_value: str = attr.ib(factory=str)
    value: SWExpressionTree = attr.ib(factory=SWExpressionTree)
    annotation: str = attr.ib(factory=str)
    old_value: VarOptValue = None
    eval_value: VarOptValue = None

    @classmethod
    def parse_from_copied_line(cls, raw_eq_line: str):
        """
        Parse raw variable equation in SW syntax.

        Ready to parse lines from a manual copy + paste operation from the
        SW equation manager window, after selection of the desired rows.

        Format looks like:
        '''
        "fan_L2"	= 465	465	1
        "D2@Z�kladn� plech1"	= "fan_L2"	465mm	0
        "D3@LinPole1"	= "D1@Skica11"	0mm	0
        "D3@Skica11"	= "D5@DIME@N015U-01-00&DV&DV 70<1>.Assembly"		0
        Tlou��ka	 = 2	2mm	0
        '''
        """
        raw_eq_terms = raw_eq_line.split("\t")
        annotation = ""
        if len(raw_eq_terms) == 4:
            name, expression, evals_to, internal = raw_eq_terms
        else:
            name, expression, evals_to, internal, annotation = raw_eq_terms
        expression = RG_CLEAN_PARTIAL_EQ.sub("", expression)
        if '"' not in name:
            logging.info(f"Found linked variable: {name} = {evals_to}")
            assert not bool(int(internal))
            name = f'"{name.strip()}"'

        if evals_to.strip():
            evals_to = SWVariable.parse(evals_to).value
        return cls(
            variable=SWVariable.parse(name),
            raw_value=expression,
            value=SWExpressionTree.parse(expression),
            annotation=annotation,
            old_value=evals_to,
        )

    @classmethod
    def parse_from_collected(cls, extracted: Tuple[str, VarOptValue]):
        """
        Parse equation in SW syntax from extracted data with SW wrapper.
        """
        raw_equation, value = extracted
        variable = equation = annotation = ""

        if "=" in raw_equation:
            variable, equation = raw_equation.split("=", 1)

        if "'" in equation:
            equation, annotation = equation.split("'", 1)

        return cls(
            variable=SWVariable.parse(variable),
            raw_value=equation,
            value=SWExpressionTree.parse(equation),
            annotation=annotation,
            old_value=value,
        )

    def solve(
        self,
        variable_values: dict,
        strict: bool = False,
        update_eval: bool = True,
    ) -> VarValue:
        """Solve equation using an external dict of values for variables."""
        result = self.value.solve(variable_values, strict)
        if isinstance(result, float):
            result = round(
                result + pow(10, -PREC_FLOAT_DECS_FINAL) / 10.0,
                PREC_FLOAT_DECS_FINAL,
            )
        # Update last evaluated value
        if update_eval:
            self.eval_value = result

        return result

    def render(self):  # pragma: no cover
        """
        Pretty printing of parsed equation.

        It looks like:
        ```
        "H2@fan1 �ez" = "fan_DN" / 2.81 :=
            VARS: ['fan_DN']
        SWExpr[DIVISION] -> '"fan_DN" / 2.81')
        |-- SWExpr[VARIABLE] -> '"fan_DN"'; V="fan_DN")
        +-- SWExpr[VALUE] -> '2.81'; V=2.81, type: float)
        ```

        """
        header = f"{self.variable} = {self.raw_value}"
        if self.annotation:
            header += f"; [Note: {self.annotation}]"
        print(header + f" := \n\tVARS: {self.value.involved_variables_names}")
        self.value.render()
        if self.eval_value is not None:
            print(f"--> {self.eval_value}")
        print("")


# if __name__ == "__main__":
#     raw = '"v5" = ("v1" / "v2") - "v3" < "v4"'
#     raw = '("v1" / "v2") - "v3" * "v4"'
#     raw = '"v1" / "v2" - "v3" * "v4"'
#     SWExpressionTree.parse(raw).render()

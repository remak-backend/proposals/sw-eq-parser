# -*- coding: cp1250 -*-
"""
Simple equation parser to analyze SW parametric variables.
"""
from .sw_equation import SWEquation, SWExpressionTree, SWVariable
from .sw_eq_collection import SWEquationCollection

__version__ = "0.1.0"

__all__ = [
    "SWEquation",
    "SWEquationCollection",
    "SWExpressionTree",
    "SWVariable",
]
